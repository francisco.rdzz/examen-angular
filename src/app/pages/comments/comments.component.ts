import { Component, Inject, OnInit, Optional } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { Observable, Subscription } from 'rxjs';
import { PagesService } from '../pages.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss'],
})
export class CommentsComponent implements OnInit {
  comments$: Observable<any[]>;
  constructor(
    private readonly pagesService: PagesService,
    public dialogRef: MatDialogRef<CommentsComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.comments$ = this.pagesService.findCommentsByPostId(this.data.IdPost);
  }

  onClose() {
    this.dialogRef.close(false);
  }
}
